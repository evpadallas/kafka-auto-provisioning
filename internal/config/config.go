package config

import (
	"log"
	"os"

	"github.com/ilyakaznacheev/cleanenv"
)

type Config struct {
	Env string `yaml:"env" env:"ENV"`
}

type Kafka struct {
	SecurityProtocol string `yaml:"security_protocol" env:"KAFKA_SECURITY_PROTOCOL"`
	TLS              `yaml:"tls"`
	SASL             `yaml:"sasl"`
}

type TLS struct {
	TLSEnabled bool   `yaml:"tls_enabled" env:"KAFKA_TLS_ENABLED"`
	CAPath     string `yaml:"ca_path" env:"KAFKA_CA_PATH"`
	CertPath   string `yaml:"cert_path" env:"KAFKA_CERT_PATH"`
	KeyPath    string `yaml:"key_path" env:"KAFKA_KEY_PATH"`
}

type SASL struct {
	SASLEnabled   bool   `yaml:"sasl_enabled" env:"KAFKA_SASL_ENABLED"`
	SASLUsername  string `yaml:"sasl_username" env:"KAFKA_SASL_USERNAME"`
	SASLPassword  string `yaml:"sasl_password" env:"KAFKA_SASL_PASSWORD"`
	SASLMechanism `yaml:"mechanism"`
}

type SASLMechanism struct {
	MechanismName string `yaml:"name" env:"KAFKA_SASL_MECHANISM"`
}

func MustLoad() {
	configPath := os.Getenv("CONFIG_PATH")
	if configPath == "" {
		log.Fatal("The Environment CONFIG_PATH is not set!")
	}

	// check if file is not exist

	if _, err := os.Stat(configPath); os.IsNotExist(err) {
		log.Fatalf("config file %s does not exists", configPath)
	}

	var cfg Config

	if err := cleanenv.Readconfig(configPath, &cfg); err != nil {
		log.Fatalf("cannot read config file: %s", err)
	}
}
